import {describe, expect, test} from '@jest/globals';
import findBestPrice from './potter';

describe('Basket with zero book', () => {
  test('the price is 0 when there is no book', () => {
    expect(findBestPrice([0,0,0,0,0])).toBe(0);
  });
});

describe('Basket with one book', () => {
  test('the price is 8 when there is one book', () => {
    expect(findBestPrice([1,0,0,0,0])).toBe(8);
    expect(findBestPrice([0,1,0,0,0])).toBe(8);
    expect(findBestPrice([0,0,1,0,0])).toBe(8);
    expect(findBestPrice([0,0,0,1,0])).toBe(8);
    expect(findBestPrice([0,0,0,0,1])).toBe(8);
  });
});

describe('Basket with two identical books', () => {
  test('the price is 16 when there are 2 identical books', () => {
    expect(findBestPrice([2,0,0,0,0])).toBe(16);
    expect(findBestPrice([0,2,0,0,0])).toBe(16);
    expect(findBestPrice([0,0,2,0,0])).toBe(16);
    expect(findBestPrice([0,0,0,2,0])).toBe(16);
    expect(findBestPrice([0,0,0,0,2])).toBe(16);
  });
});

describe('Basket with two different books', () => {
  test('the price is 15.2 (2*8*.95) when there are 2 different books', () => {
    expect(findBestPrice([1,1,0,0,0])).toBe(15.2);
    expect(findBestPrice([1,0,1,0,0])).toBe(15.2);
    expect(findBestPrice([1,0,0,1,0])).toBe(15.2);
    expect(findBestPrice([1,0,0,0,1])).toBe(15.2);
    expect(findBestPrice([0,1,1,0,0])).toBe(15.2);
    expect(findBestPrice([0,1,0,1,0])).toBe(15.2);
    expect(findBestPrice([0,1,0,0,1])).toBe(15.2);
    expect(findBestPrice([0,0,1,1,0])).toBe(15.2);
    expect(findBestPrice([0,0,1,0,1])).toBe(15.2);
    expect(findBestPrice([0,0,0,1,1])).toBe(15.2);
  });
});

describe('Basket with 3 identical books', () => {
  test('the price is 24 (8 * 3) when there are 3 identical books', () => {
    expect(findBestPrice([3,0,0,0,0])).toBe(24);
    expect(findBestPrice([0,3,0,0,0])).toBe(24);
    expect(findBestPrice([0,0,3,0,0])).toBe(24);
    expect(findBestPrice([0,0,0,3,0])).toBe(24);
    expect(findBestPrice([0,0,0,0,3])).toBe(24);
  });
});

describe('Basket with 3 different books', () => {
  test('the price is 21.6 (8 * 3 * .9) when there are 3 different books', () => {
    expect(findBestPrice([1,1,1,0,0])).toBe(21.6);
    expect(findBestPrice([0,1,1,1,0])).toBe(21.6);
    expect(findBestPrice([0,0,1,1,1])).toBe(21.6);
    expect(findBestPrice([1,0,1,0,1])).toBe(21.6);
    expect(findBestPrice([1,1,0,0,1])).toBe(21.6);
  });
});

describe('Basket with 2 of the same book and 1 other book', () => {
  test('the price is 23.2 (2*8*.95 + 8)', () => {
    expect(findBestPrice([2,1,0,0,0])).toBe(23.2);
    expect(findBestPrice([2,0,1,0,0])).toBe(23.2);
    expect(findBestPrice([2,0,0,1,0])).toBe(23.2);
    expect(findBestPrice([2,0,0,0,1])).toBe(23.2);
    expect(findBestPrice([0,2,1,0,0])).toBe(23.2);
    expect(findBestPrice([0,2,0,1,0])).toBe(23.2);
    expect(findBestPrice([0,2,0,0,1])).toBe(23.2);
    expect(findBestPrice([0,0,2,1,0])).toBe(23.2);
    expect(findBestPrice([0,0,2,0,1])).toBe(23.2);
    expect(findBestPrice([0,0,0,2,1])).toBe(23.2);
    expect(findBestPrice([0,0,0,1,2])).toBe(23.2);
  });
});

describe('Basket with 4 identical books', () => {
  test('the price is 32 (8 * 4) when there are 4 different books', () => {
    expect(findBestPrice([4,0,0,0,0])).toBe(32);
    expect(findBestPrice([0,4,0,0,0])).toBe(32);
    expect(findBestPrice([0,0,4,0,0])).toBe(32);
    expect(findBestPrice([0,0,0,4,0])).toBe(32);
    expect(findBestPrice([0,0,0,0,4])).toBe(32);
  });
});

describe('Basket with 4 different books', () => {
  test('the price is 25.6 (8 * 4 * .8) when there are 4 different books', () => {
    expect(findBestPrice([1,1,1,1,0])).toBe(25.6);
    expect(findBestPrice([1,1,1,0,1])).toBe(25.6);
    expect(findBestPrice([1,1,0,1,1])).toBe(25.6);
    expect(findBestPrice([1,0,1,1,1])).toBe(25.6);
    expect(findBestPrice([0,1,1,1,1])).toBe(25.6);
  });
});

describe('Basket with 2 of the same book and 2 other distincts books', () => {
  test('the price is 29.6 (3 * 8 * .9 + 8) when there are 2 of the same book and 2 other distinct books', () => {
    expect(findBestPrice([2,1,1,0,0])).toBe(29.6);
    expect(findBestPrice([2,0,1,1,0])).toBe(29.6);
    expect(findBestPrice([2,0,0,1,1])).toBe(29.6);
    expect(findBestPrice([1,2,1,0,0])).toBe(29.6);
    expect(findBestPrice([1,1,2,0,0])).toBe(29.6);
    expect(findBestPrice([1,1,0,2,0])).toBe(29.6);
    expect(findBestPrice([1,0,1,0,2])).toBe(29.6);
  });
});

describe('Basket with 2 of the same book and 2 of another distinct book', () => {
  test('the price is 30.4 (2 * 8 * .95 + 2 * 8 * .95) when there are 3 of the same book and 1 other distinct books', () => {
    expect(findBestPrice([2,2,0,0,0])).toBe(30.4);
    expect(findBestPrice([2,0,2,0,0])).toBe(30.4);
    expect(findBestPrice([2,0,0,2,0])).toBe(30.4);
    expect(findBestPrice([0,2,2,0,0])).toBe(30.4);
    expect(findBestPrice([0,2,0,2,0])).toBe(30.4);
    expect(findBestPrice([0,2,0,0,2])).toBe(30.4);
    expect(findBestPrice([0,0,2,2,0])).toBe(30.4);
    expect(findBestPrice([0,0,0,2,2])).toBe(30.4);
  });
});

describe('Basket with 3 of the same book and 1 other distinct books', () => {
  test('the price is 31.2 (2*8*.95 + 2*8)when there are 3 of the same book and 1 other distinct books', () => {
    expect(findBestPrice([3,1,0,0,0])).toBe(31.2);
    expect(findBestPrice([3,0,1,0,0])).toBe(31.2);
    expect(findBestPrice([3,0,0,0,1])).toBe(31.2);
    expect(findBestPrice([1,3,0,0,0])).toBe(31.2);
    expect(findBestPrice([0,1,3,0,0])).toBe(31.2);
    expect(findBestPrice([0,0,0,3,1])).toBe(31.2);
    expect(findBestPrice([0,0,1,0,3])).toBe(31.2);
  });
});

describe('Basket with 5 identical books', () => {
  test('the price is 40 (8 * 5) when there are 5 identical books', () => {
    expect(findBestPrice([5,0,0,0,0])).toBe(40);
    expect(findBestPrice([0,5,0,0,0])).toBe(40);
    expect(findBestPrice([0,0,5,0,0])).toBe(40);
    expect(findBestPrice([0,0,0,5,0])).toBe(40);
    expect(findBestPrice([0,0,0,0,5])).toBe(40);
  });
});

describe('Basket with 5 different books', () => {
  test('the price is 30 (8 * 5 *.75) when there are 5 different books', () => {
    expect(findBestPrice([1,1,1,1,1])).toBe(30);
  });
});

describe('Basket with 4 of the same book and 1 other distinct book', () => {
  test('the price is 39.2 (8 * 2 *.95 + 3*8) when there are 4 + 1 books', () => {
    expect(findBestPrice([4,0,0,0,1])).toBe(39.2);
    expect(findBestPrice([1,0,0,0,4])).toBe(39.2);
    expect(findBestPrice([0,0,1,0,4])).toBe(39.2);
  });
});

describe('Basket of the example', () => {
  test('the price is 51.2', () => {
    expect(findBestPrice([2,2,2,1,1])).toBe(51.2);
    expect(findBestPrice([1,1,2,2,2])).toBe(51.2);
  });
});

describe('Basket with 3+3+3+2+2 books', () => {
  test('the price is 81.2 (51.2 + 30)', () => {
    expect(findBestPrice([3,3,3,2,2])).toBe(81.2);
    expect(findBestPrice([2,2,3,3,3])).toBe(81.2);
    expect(findBestPrice([2,3,3,3,2])).toBe(81.2);
    expect(findBestPrice([3,2,3,2,3])).toBe(81.2);
  });
});
