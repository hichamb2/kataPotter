# My Kata Potter implementation

For this exercise, our function will accept an array of 5 numbers representing how many of each book we have in the basket.
The number at index 0 will be the total of first book, the one at index 1 will be the total of second book, etc...  

This code has been written using the TDD Method.

The first test cases are quite simple. Up to the test case with 3 different books, it is basically a matter of multiplying the unit price by the number of books and applying the discount.

From the test case 2+1 books, we have to apply a discount on a part of the books, and no discount on the remaining books.
From 2+1+1 books, we can have different combinations of books for a given basket, with a potentially different total price.
The goal is then to find those combinations and compute the total price for each of them.
The idea is to extract the biggest set of distinct books we can find, and to form another set with the remaining books. This will form our first combination of books.
Then we perform some permutations between those two sets. Given than we only have 3 books at this point, we can't have more than a permutation : we can simply swap the first book that is included in the first set but not in the second set.
This implementation is a major step as the amount of code added is significant.

The following iterations (appart from the 3+1 iteration), up to the 4+1 are small additions, such as added or modified if clauses so that the right code is executed for the tests cases

Iteration for 3+1 books modifies the code so that we can handle permutations with 4 books and cases were no permutation are possible.

After the 4+1 iteration, we performed a refactoring. The code was starting to feel complicated with multiple branching. And with the lattest additions to the method calculating the permutations, we should be able to manage the previous test cases with little changes.

At the iteration 2+2+2+1+1 books, it became apparent that there was a mistake in the permutation generation code. The mistake was that we used index 1 and 2 to access the first two set of books for a basket combination instead of 0 and 1. All tests were successfully executed until now, that means there was a loophole in the test cases, or that we implemented more code than necessary.

For the last test case, we modified the code to handle cases with several full sets of book. We iterate through the sets of books until we are able to make a permutation. 
