export default function findBestPrice(books:number[]): number 
{
    let totalPrice=0;
    const unitPrice=8;
    let bookCount=books.reduce((prev,n)=>prev+n,0);
    const maxOfOneBook=Math.max(...books);

        if(bookCount===maxOfOneBook)
        {
            totalPrice = unitPrice*bookCount;
        }
        else
        {
            return getBestPrice(books);
        }

    return totalPrice;
}

function getSingleBookCount(books:number[])
{
    let count=0;
    for (let i=0;i<books.length;i++)
    {
        if(books[i]===1)count++;
    }
    return count; 
}

function getBestPrice(books)
{
    const combination1:number[][]=[];

    const remainingBooks=[...books];
    while(Math.max(...remainingBooks)>1)
    {
        const combo=[0,0,0,0,0];
        
        for (let i=0; i<remainingBooks.length; i++)
        {
            if(remainingBooks[i]>0)
            {
                remainingBooks[i]--;
                combo[i]=1;
            }
        }
        combination1.push(combo);
    }
    
    combination1.push(remainingBooks);

    const combination2=makePermutation(combination1);
    
    const price1=computePrice(combination1);
    if(combination2.length>0)
    {
        return Math.min(price1,computePrice(combination2));
    }

    return price1;
}

function makePermutation(books:number[][]):number[][]
{
    const permutBooks=[...books];

    let indice=0;

    let count1=getSingleBookCount(permutBooks[indice]);
    let count2=permutBooks[indice+1]?getSingleBookCount(permutBooks[indice+1]):0;
    if(count1==count2) indice++;
    for (let i=0; permutBooks[indice]&&i<permutBooks[indice].length; i++)
    {

        if(permutBooks[indice+1])console.log(permutBooks[indice][i] !== permutBooks[indice+1][i]);
        if(permutBooks[indice+1] && (permutBooks[indice][i] !== permutBooks[indice+1][i]))
        {
            permutBooks[indice]=[...permutBooks[indice]];   
            permutBooks[indice+1]=[...permutBooks[indice+1]];
            const temp=permutBooks[indice][i];
            permutBooks[indice][i] = permutBooks[indice+1][i];
            permutBooks[indice+1][i] = temp;

            return permutBooks;
        }
    }

    return [];
}

function computePrice(booksCombination:number[][]):number
{
    let total=0;
    for( let books of booksCombination)
    {
        const count=getSingleBookCount(books);
        total+=count*8*getDiscountRatio(count);
    }

    return total
}

function getDiscountRatio(bookCount:number):number
{
    switch(bookCount)
    {
        case 2 :
            return .95;
        case 3 :
            return .9;
        case 4 :
            return .8;
        case 5 :
            return .75;
        default:
            return 1;
    }
}